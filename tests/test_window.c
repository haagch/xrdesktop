/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>
#include <glib.h>
#include <xrd.h>

static gboolean
_similar (float a, float b)
{
  return fabs ((double) a - (double) b) < 0.01;
}

static VkExtent2D extent;

static float    expected_mouse_x = 0;
static float    expected_mouse_y = 0;
static gboolean success = FALSE;
static void
_move_cursor_cb (XrdShell *shell, XrdMoveCursorEvent *event, gpointer *_)
{
  (void) shell;
  (void) _;
  (void) event;

  float x = event->position->x;
  float y = (float) extent.height - event->position->y;

  g_print ("move: %f, %f\n", (double) x, (double) y);
  if (_similar (expected_mouse_x, x) || !_similar (expected_mouse_y, y))
    success = TRUE;
  else
    {
      g_print ("Error: mouse move to unexpected location!\n");
      g_print ("IS    : %f, %f\n", (double) event->position->x, (double) y);
      g_print ("SHOULD: %f %f\n", (double) expected_mouse_x,
               (double) expected_mouse_y);
      success = FALSE;
    }
}

static gboolean
_test_move (float     left,
            float     bottom,
            float     ppm,
            float     dist,
            XrdShell *shell,
            int       x,
            int       y)
{
  g_print ("Test %d %d\n", x, y);
  graphene_vec3_t eye;
  graphene_vec3_init (&eye, 0, 0, 0);

  graphene_vec3_t to;
  graphene_vec3_init (&to, left + (float) x / ppm, bottom + (float) y / ppm,
                      dist);

  graphene_matrix_t pose;
  graphene_matrix_init_look_at (&pose, &eye, &to, graphene_vec3_y_axis ());

  expected_mouse_x = (float) x;
  expected_mouse_y = (float) y;

  G3kContext    *g3k = xrd_shell_get_g3k (shell);
  G3kController *controller = g3k_context_init_dummy_controller (g3k);
  GxrController *gxr_controller = g3k_controller_get_controller (controller);

  GxrPoseEvent event = {
    .active = TRUE,
    .controller = 0,
    .device_connected = TRUE,
    .valid = TRUE,
    .pose = pose,
  };
  gxr_controller_update_pointer_pose (gxr_controller, &event);
  G3kObjectManager *manager = g3k_context_get_manager (g3k);
  g3k_object_manager_update_controller (manager, controller);

  g_object_unref (controller);

  return success;
}

static int
_test_window ()
{
  XrdShell *shell = xrd_shell_new ();
  g_assert_nonnull (shell);

  g_signal_connect (shell, "move-cursor-event", (GCallback) _move_cursor_cb,
                    NULL);

  G3kContext    *g3k = xrd_shell_get_g3k (shell);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanTexture *texture
    = gulkan_texture_new_from_resource (gulkan, "/res/cat.jpg",
                                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                        TRUE);
  if (!texture)
    return EXIT_FAILURE;

  extent = gulkan_texture_get_extent (texture);
  float aspect = (float) extent.width / (float) extent.height;

  float window_width_meter = 2.0;
  float window_height_meter = window_width_meter / aspect;
  float ppm = (float) extent.width / window_width_meter;

  graphene_size_t size_meters = {window_width_meter, window_height_meter};

  XrdWindow *window = xrd_window_new (xrd_shell_get_g3k (shell), "win.", NULL,
                                      extent, &size_meters);

  float left = 0.5f;
  float right = left + window_width_meter;
  float bottom = 1.5f;
  float top = bottom + window_height_meter;

  float dist = -3.f;

  graphene_point3d_t point = {
    .x = left + (right - left) / 2.f,
    .y = bottom + (top - bottom) / 2.f,
    .z = dist,
  };

  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, &point);
  g3k_object_set_matrix (G3K_OBJECT (window), &transform);

  g3k_plane_set_texture (G3K_PLANE (window), texture);

  xrd_shell_add_window (shell, window, NULL, TRUE, NULL);

  GulkanDevice *device = gulkan_context_get_device (gulkan);
  gulkan_device_wait_idle (device);

  g3k_context_render (g3k);
  gulkan_device_wait_idle (device);

  g3k_context_render (g3k);
  gulkan_device_wait_idle (device);

  const int ms_delay = 150;

  if (!_test_move (left, bottom, ppm, dist, shell, 100, 100))
    {
      g_object_unref (shell);
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  if (!_test_move (left, bottom, ppm, dist, shell, 10, 300))
    {
      g_object_unref (shell);
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  if (!_test_move (left, bottom, ppm, dist, shell, 400, 10))
    {
      g_object_unref (shell);
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  g_object_unref (shell);

  return EXIT_SUCCESS;
}

int
main ()
{
  return _test_window ();
}
