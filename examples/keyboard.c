/*
 * xrdesktop
 * Copyright 2021 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * Author: Remco Kranenburg <remco@burgsoft.nl>
 * SPDX-License-Identifier: MIT
 */

#include <glib-unix.h>
#include <glib.h>
#include <locale.h>

#include <xrd.h>

typedef struct Example
{
  GMainLoop   *loop;
  XrdShell    *shell;
  G3kKeyboard *keyboard;

  G3kButton *quit_button;
  G3kButton *keyboard_button;

  guint64 click_source;
} Example;

static gboolean
_sigint_cb (gpointer _self)
{
  Example *self = (Example *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static void
_button_quit_press_cb (XrdWindow     *window,
                       GxrController *controller,
                       gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;

  g_main_loop_quit (self->loop);
}

static void
_button_keyboard_press_cb (XrdWindow     *window,
                           GxrController *controller,
                           gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;

  if (g3k_keyboard_is_visible (self->keyboard))
    g3k_keyboard_hide (self->keyboard);
  else
    g3k_keyboard_show (self->keyboard);
}

static void
_init_buttons (Example *self)
{
  graphene_point3d_t button_pos = {.x = -1.5f, .y = 0.0f, .z = -2.5f};

  graphene_size_t size_meters = {.6f, .6f};

  G3kContext *g3k = xrd_shell_get_g3k (self->shell);

  gchar *quit_str[] = {"Quit"};
  self->quit_button = g3k_button_new (g3k, &size_meters);
  if (!self->quit_button)
    return;

  g3k_button_set_text (self->quit_button, 1, quit_str);

  xrd_shell_add_button (self->shell, self->quit_button, &button_pos,
                        G_CALLBACK (_button_quit_press_cb), self, NULL);

  button_pos.x += 0.6f;

  gchar *keyboard_str[] = {"Toggle", "Cool", "Keyboard"};
  self->keyboard_button = g3k_button_new (g3k, &size_meters);
  if (!self->keyboard_button)
    return;

  g3k_button_set_text (self->keyboard_button, 3, keyboard_str);

  xrd_shell_add_button (self->shell, self->keyboard_button, &button_pos,
                        G_CALLBACK (_button_keyboard_press_cb), self, NULL);
}

static void
_cleanup (Example *self)
{
  self->click_source = 0;

  g_object_unref (self->shell);
  self->shell = NULL;

  g_print ("Cleaned up!\n");
}

static void
keyboard_pressed_cb (G3kKeyboard *keyboard,
                     G3kKeyEvent *event,
                     gpointer     user_data)
{
  (void) keyboard;
  (void) user_data;
  g_print ("Yay, key %s was pressed!\n", event->string);
}

static gboolean
_init_example (Example *self)
{
  if (!self->shell)
    {
      g_printerr ("XrdShell did not initialize correctly.\n");
      return FALSE;
    }

  _init_buttons (self);

  self->keyboard = xrd_shell_get_keyboard (self->shell);

  g_signal_connect (self->keyboard, "key-press-event",
                    G_CALLBACK (keyboard_pressed_cb), NULL);

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  return TRUE;
}

gint
main ()
{
  /* Set all locale-related variables to their values as specified in the
   * environment. This way, you can configure keyboard layout by setting
   * the LANG environment variable.
   */
  setlocale (LC_ALL, "");

  Example self = {
    .loop = g_main_loop_new (NULL, FALSE),
    .shell = xrd_shell_new (),
  };

  if (!_init_example (&self))
    return EXIT_FAILURE;

  /* start glib main loop */
  g_main_loop_run (self.loop);

  _cleanup (&self);

  g_main_loop_unref (self.loop);

  return EXIT_SUCCESS;
}
