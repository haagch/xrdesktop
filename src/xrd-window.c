/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-window.h"

#include <gdk/gdk.h>
#include <stdalign.h>

typedef struct
{
  alignas (16) float color[4];
  alignas (4) bool flip_y;

  // rect of actual window content on the submitted texture.
  // note: y points up in texture coordinates.
  alignas (4) float bl_x;
  alignas (4) float bl_y;
  alignas (4) float tr_x;
  alignas (4) float tr_y;
} XrdWindowShadingUniformBuffer;

typedef struct
{
  alignas (32) float mvp[2][16];
  alignas (32) float mv[2][16];
  alignas (16) float m[16];
} XrdWindowUniformBuffer;

/**
 * _XrdWindowPrivate:
 * @native: A native pointer to a window struct from a window manager.

 * @title: A window title.
 * @quad_width: The "base window" has a width of 1 unit (normalized coordnates).
 The width is a factor applied to the "base window".
 *              E.g. if the window scene object is globally scaled at 1.0, a
 width of 2 indicates a width of 2 meter.
 *              This attribute is intended to be constant. Scale the window by
 scaling the scene object.
 * @width_pixel: The width in pixels of the content rendered on this window.
 * @height_pixel: The height in pixels of the content rendered on this window.
 * @child_windows: A list of XrdWindowData that are pinned on top of this window
 and follows this window's position and scaling.
 * @parent_window: The parent window, %NULL if the window does not have a
 parent.
 * @reset_transform: The transformation that the window will be reset to.
 * @pinned: Whether the window will be visible in pinned only mode.
 * @texture: Cache of the currently rendered texture.
 * @rect: The rectangle on the @texture used to render window content.
 * @xrd_window: A pointer to the #XrdWindow this XrdWindowData belongs to.
 *
 **/
struct _XrdWindow
{
  G3kPlane plane;

  gboolean flip_y;

  GulkanUniformBuffer          *shading_buffer;
  XrdWindowShadingUniformBuffer shading_buffer_data;

  gpointer native;

  gchar *title;

  GSList    *children;
  XrdWindow *parent;

  gboolean pinned;

  XrdWindowRect rect;
};

enum
{
  PROP_TITLE = 1,
  PROP_NATIVE,
  N_PROPERTIES
};

enum
{
  MOTION_NOTIFY_EVENT,
  BUTTON_PRESS_EVENT,
  BUTTON_RELEASE_EVENT,
  SHOW,
  DESTROY,
  SCROLL_EVENT,
  LAST_SIGNAL
};

static guint       window_signals[LAST_SIGNAL] = {0};
static GParamSpec *window_properties[N_PROPERTIES] = {
  NULL,
};

G_DEFINE_TYPE (XrdWindow, xrd_window, G3K_TYPE_PLANE)

static void
_update_rect_ubo (XrdWindow *self);

static void
xrd_window_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  XrdWindow *self = XRD_WINDOW (object);

  switch (property_id)
    {
      case PROP_TITLE:
        if (self->title)
          g_free (self->title);
        self->title = g_strdup (g_value_get_string (value));
        break;
      case PROP_NATIVE:
        self->native = g_value_get_pointer (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
xrd_window_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  XrdWindow *self = XRD_WINDOW (object);

  switch (property_id)
    {
      case PROP_TITLE:
        g_value_set_string (value, self->title);
        break;
      case PROP_NATIVE:
        g_value_set_pointer (value, self->native);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        break;
    }
}

static void
_finalize (GObject *gobject);

static void
_draw (G3kObject *self, VkCommandBuffer cmd_buffer);

gboolean
_update_selection (G3kObject *self, G3kSelection *selection);

static VkExtent2D
_get_content_extent (G3kPlane *plane)
{
  XrdWindow *self = XRD_WINDOW (plane);

  g_assert (self->rect.tr.x > self->rect.bl.x);
  g_assert (self->rect.tr.y > self->rect.bl.y);

  VkExtent2D extent = {
    .width = self->rect.tr.x - self->rect.bl.x,
    .height = self->rect.tr.y - self->rect.bl.y,
  };
  return extent;
}

static void
xrd_window_class_init (XrdWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = _finalize;

  object_class->set_property = xrd_window_set_property;
  object_class->get_property = xrd_window_get_property;

  G3kObjectClass *g3k_object_class = G3K_OBJECT_CLASS (klass);
  g3k_object_class->draw = _draw;
  g3k_object_class->update_selection = _update_selection;

  G3kPlaneClass *plane_class = G3K_PLANE_CLASS (klass);
  plane_class->get_content_extent = _get_content_extent;

  window_signals[MOTION_NOTIFY_EVENT]
    = g_signal_new ("motion-notify-event", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

  window_signals[BUTTON_PRESS_EVENT]
    = g_signal_new ("button-press-event", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

  window_signals[BUTTON_RELEASE_EVENT]
    = g_signal_new ("button-release-event", G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
                    GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

  window_signals[SHOW] = g_signal_new ("show", G_TYPE_FROM_CLASS (klass),
                                       G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL,
                                       G_TYPE_NONE, 0);

  window_signals[DESTROY] = g_signal_new ("destroy", G_TYPE_FROM_CLASS (klass),
                                          G_SIGNAL_RUN_CLEANUP
                                            | G_SIGNAL_NO_RECURSE
                                            | G_SIGNAL_NO_HOOKS,
                                          0, NULL, NULL, NULL, G_TYPE_NONE, 0);

  window_signals[SCROLL_EVENT] = g_signal_new ("scroll-event",
                                               G_TYPE_FROM_CLASS (klass),
                                               G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                               NULL, G_TYPE_NONE, 1,
                                               GDK_TYPE_EVENT
                                                 | G_SIGNAL_TYPE_STATIC_SCOPE);

  window_properties[PROP_TITLE] = g_param_spec_string ("title", "Title",
                                                       "Title of the Window.",
                                                       "Untitled",
                                                       G_PARAM_CONSTRUCT_ONLY
                                                         | G_PARAM_READWRITE);

  window_properties[PROP_NATIVE]
    = g_param_spec_pointer ("native", "Native Window Handle",
                            "A pointer to an (opaque) native window struct.",
                            G_PARAM_CONSTRUCT | G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPERTIES,
                                     window_properties);
}

static void
xrd_window_init (XrdWindow *self)
{
  self->shading_buffer_data.flip_y = FALSE;
  self->title = NULL;
  self->children = NULL;
  self->parent = NULL;
  self->native = NULL;
  self->rect.tr.x = 0;
  self->rect.tr.y = 0;
  self->rect.bl.x = 0;
  self->rect.bl.y = 0;
  self->pinned = FALSE;
}

XrdWindow *
xrd_window_new (G3kContext      *g3k,
                const gchar     *title,
                gpointer         native,
                VkExtent2D       size_pixels,
                graphene_size_t *size_meters)
{
  XrdWindow *self = (XrdWindow *) g_object_new (XRD_TYPE_WINDOW, "title", title,
                                                "native", native, NULL);

  self->rect = (XrdWindowRect) {
    .bl = {
      .x = 0,
      .y = 0,
    },
    .tr = {
      .x = size_pixels.width,
      .y = size_pixels.height,
    }
  };

  VkDeviceSize ubo_size = sizeof (XrdWindowUniformBuffer);

  G3kRenderer *renderer = g3k_context_get_renderer (g3k);
  G3kPipeline *pipeline = g3k_renderer_get_pipeline (renderer, "window");

  if (!g3k_object_initialize (G3K_OBJECT (self), g3k, pipeline, ubo_size))
    {
      g_printerr ("Coult not init XrdWindow.\n");
      g_object_unref (self);
      return NULL;
    }

  VkDeviceSize shading_ubo_size = sizeof (XrdWindowShadingUniformBuffer);

  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  GulkanDevice  *device = gulkan_context_get_device (gulkan);
  self->shading_buffer = gulkan_uniform_buffer_new (device, shading_ubo_size);
  if (!self->shading_buffer)
    {
      g_printerr ("Coult not init XrdWindow shading buffer.\n");
      g_object_unref (self);
      return NULL;
    }

  GulkanDescriptorSet *descriptor_set
    = g3k_object_get_descriptor_set (G3K_OBJECT (self));

  gulkan_descriptor_set_update_buffer (descriptor_set, 2, self->shading_buffer);
  _update_rect_ubo (self);

  GulkanUniformBuffer *lights = g3k_renderer_get_lights_buffer (renderer);
  gulkan_descriptor_set_update_buffer (descriptor_set, 3, lights);

  xrd_window_reset_color (self);

  g3k_plane_set_mesh_size (G3K_PLANE (self), size_meters);

  return self;
}

static void
_finalize (GObject *gobject)
{
  XrdWindow *self = XRD_WINDOW (gobject);

  g_clear_object (&self->shading_buffer);

  g_free (self->title);

  G_OBJECT_CLASS (xrd_window_parent_class)->finalize (gobject);
}

static void
_update_ubo (XrdWindow *self)
{
  XrdWindowUniformBuffer ub = {0};

  graphene_matrix_t m_matrix;
  g3k_object_get_matrix (G3K_OBJECT (self), &m_matrix);
  graphene_matrix_to_float (&m_matrix, ub.m);

#if 0
  graphene_point3d_t p;
  graphene_ext_matrix_get_translation_point3d (&m_matrix, &p);
  g_debug ("ubo model at %f %f %f", p.x, p.y, p.z);
#endif

  G3kContext        *context = g3k_object_get_context (G3K_OBJECT (self));
  graphene_matrix_t *views = g3k_context_get_views (context);
  graphene_matrix_t *projections = g3k_context_get_projections (context);

  for (uint32_t eye = 0; eye < 2; eye++)
    {
      graphene_matrix_t mv_matrix;
      graphene_matrix_multiply (&m_matrix, &views[eye], &mv_matrix);
      graphene_matrix_to_float (&mv_matrix, ub.mv[eye]);

      graphene_matrix_t mvp_matrix;
      graphene_matrix_multiply (&mv_matrix, &projections[eye], &mvp_matrix);
      graphene_matrix_to_float (&mvp_matrix, ub.mvp[eye]);
    }

  g3k_object_update_transformation_ubo (G3K_OBJECT (self), &ub);
}

static void
_draw (G3kObject *obj, VkCommandBuffer cmd_buffer)
{
  XrdWindow *self = XRD_WINDOW (obj);
  if (!g3k_plane_has_texture (G3K_PLANE (self)))
    {
      /* g_warning ("Trying to draw window with no texture.\n"); */
      return;
    }

  _update_ubo (self);

  g3k_object_bind (obj, cmd_buffer);

  GulkanVertexBuffer *vb = g3k_plane_get_vertex_buffer (G3K_PLANE (self));
  g_assert (vb);
  gulkan_vertex_buffer_draw (vb, cmd_buffer);
}

void
xrd_window_set_color (XrdWindow *self, const graphene_vec4_t *color)
{
  graphene_vec4_to_float (color, self->shading_buffer_data.color);
  gulkan_uniform_buffer_update (self->shading_buffer,
                                (gpointer) &self->shading_buffer_data);
}

static void
_update_rect_ubo (XrdWindow *self)
{
  XrdWindowShadingUniformBuffer *ubo = &self->shading_buffer_data;

  VkExtent2D     extent;
  GulkanTexture *texture = g3k_plane_get_texture (G3K_PLANE (self));
  if (texture)
    extent = gulkan_texture_get_extent (texture);
  else
    {
      // if no texture has been submitted, we won't render anyway
      extent.width = self->rect.tr.x - self->rect.bl.x;
      extent.height = self->rect.tr.y - self->rect.bl.y;
    }

  ubo->bl_x = (float) self->rect.bl.x / (float) extent.width;
  ubo->bl_y = (float) self->rect.bl.y / (float) extent.height;
  ubo->tr_x = (float) self->rect.tr.x / (float) extent.width;
  ubo->tr_y = (float) self->rect.tr.y / (float) extent.height;

  gulkan_uniform_buffer_update (self->shading_buffer, (gpointer) ubo);
}

void
xrd_window_set_rect (XrdWindow *self, XrdWindowRect *rect)
{
  if (rect)
    {
      self->rect = *rect;
      // rect can be set before the window is initialized
      if (self->shading_buffer)
        _update_rect_ubo (self);
    }
}

XrdWindowRect *
xrd_window_get_rect (XrdWindow *self)
{
  return &self->rect;
}

/**
 * xrd_window_get_pixel_intersection:
 * @self: The #XrdWindow
 * @intersection_3d: A #graphene_point3d_t intersection point in meters.
 * @intersection_pixels: Intersection in object coordinates with origin at
 * top-left in pixels.
 */
void
xrd_window_get_pixel_intersection (XrdWindow          *self,
                                   graphene_point3d_t *intersection_3d,
                                   graphene_point_t   *intersection_pixels)
{
  /* transform intersection point to origin */
  graphene_point_t intersection_2d_point;
  g3k_object_get_object_space_intersection (G3K_OBJECT (self), intersection_3d,
                                            &intersection_2d_point);

  graphene_vec2_t intersection_2d_vec;
  graphene_point_to_vec2 (&intersection_2d_point, &intersection_2d_vec);

  /* normalize coordinates to [0 - 1, 0 - 1] */
  graphene_size_t size_meters
    = g3k_plane_get_global_size_meters (G3K_PLANE (self));

  graphene_vec2_t size_meters_vec;
  graphene_vec2_init (&size_meters_vec, size_meters.width, size_meters.height);

  graphene_vec2_divide (&intersection_2d_vec, &size_meters_vec,
                        &intersection_2d_vec);

  /* move origin from center to corner of overlay */
  graphene_vec2_t center_normalized;
  graphene_vec2_init (&center_normalized, 0.5f, 0.5f);

  graphene_vec2_add (&intersection_2d_vec, &center_normalized,
                     &intersection_2d_vec);

  /* invert y axis */
  graphene_vec2_init (&intersection_2d_vec,
                      graphene_vec2_get_x (&intersection_2d_vec),
                      1.0f - graphene_vec2_get_y (&intersection_2d_vec));

  // TODO code path for when borders are rendered
  /* scale to pixel coordinates */

  VkExtent2D extent = _get_content_extent (G3K_PLANE (self));

  graphene_vec2_t content_size_pixels_vec;
  graphene_vec2_init (&content_size_pixels_vec, (float) extent.width,
                      (float) extent.height);

  graphene_vec2_multiply (&intersection_2d_vec, &content_size_pixels_vec,
                          &intersection_2d_vec);

  // intersection on the rect, but should be on the whole texture
  graphene_vec2_t offset;
  graphene_vec2_init (&offset, (float) self->rect.bl.x,
                      (float) self->rect.bl.y);
  graphene_vec2_add (&intersection_2d_vec, &offset, &intersection_2d_vec);

  /* return point_t */
  graphene_point_init_from_vec2 (intersection_pixels, &intersection_2d_vec);
}

static void
_inherit_state_from_parent (XrdWindow *parent, XrdWindow *child)
{
  /* child window should inherit pinned status */
  xrd_window_set_pin (child, xrd_window_is_pinned (parent), FALSE);
  /* we don't know if shell is in pinned only modus, but it is better to
   * inherit visibility from parent anyway. */
  g3k_object_set_visibility (G3K_OBJECT (child),
                             g3k_object_is_visible (G3K_OBJECT (parent)));
}

gboolean
_update_selection (G3kObject *self, G3kSelection *selection)
{
  graphene_matrix_t window_transformation;
  g3k_object_get_matrix (G3K_OBJECT (self), &window_transformation);
  g3k_object_set_matrix (G3K_OBJECT (selection), &window_transformation);

  graphene_size_t size_meters = g3k_plane_get_mesh_size (G3K_PLANE (self));

  g3k_selection_set_quad (selection, size_meters.width, size_meters.height);

  return TRUE;
}

/**
 * xrd_window_add_child:
 * @self: The #XrdWindow
 * @child: An already existing window.
 * @offset_center: The offset of the child window's center to the parent
 * window's center in pixels.
 *
 * x axis points right, y axis points up.
 */
void
xrd_window_add_child (XrdWindow *self, XrdWindow *child)
{
  if (!child)
    return;

  self->children = g_slist_append (self->children, child);
  child->parent = self;

  _inherit_state_from_parent (self, child);
}

void
xrd_window_set_selection_color (XrdWindow *self, gboolean is_selected)
{
  graphene_vec4_t color;
  if (is_selected)
    {
      graphene_vec4_init (&color, 0.0f, 0.0f, 1.0f, 1.0f);
    }
  else
    {
      graphene_vec4_init (&color, 0.1f, 0.1f, 0.1f, 1.0f);
    }
  xrd_window_set_color (self, &color);
}

void
xrd_window_reset_color (XrdWindow *self)
{
  graphene_vec4_t unmarked_color;
  graphene_vec4_init (&unmarked_color, 1.f, 1.f, 1.f, 1.0f);
  xrd_window_set_color (self, &unmarked_color);
}

void
xrd_window_set_flip_y (XrdWindow *self, gboolean flip_y)
{
  self->flip_y = flip_y;
  self->shading_buffer_data.flip_y = flip_y;

  gulkan_uniform_buffer_update (self->shading_buffer,
                                (gpointer) &self->shading_buffer_data);
}

/**
 * xrd_window_set_pin:
 * @self: The #XrdWindow
 * @pinned: The pin status to set this window to
 * @hide_unpinned: If TRUE, the window will be hidden if it is unpinned, and
 * shown if it is pinned. This corresponds to the "show only pinned windows"
 * mode set up in #XrdShell.
 * If FALSE, windows are always shown.
 * Note that @hide_unpinned only determines initial visibility, and does not
 * keep track of further mode changes.
 */
void
xrd_window_set_pin (XrdWindow *self, gboolean pinned, gboolean hide_unpinned)
{
  if (hide_unpinned)
    g3k_object_set_visibility (G3K_OBJECT (self), pinned);
  else
    g3k_object_set_visibility (G3K_OBJECT (self), TRUE);

  self->pinned = pinned;
}

gboolean
xrd_window_is_pinned (XrdWindow *self)
{
  return self->pinned;
}

/**
 * xrd_window_close:
 * @self: The #XrdWindow
 * MUST be called when destroying a window to free its resources.
 */
void
xrd_window_close (XrdWindow *self)
{
  /* cleanup: If we are a child, we remove ourselves from the parent's children
   * list */
  if (self->parent != NULL)
    self->parent->children = g_slist_remove (self->parent->children, self);

  /* cleanup: If we are a parent, we set our childrens' parent ptr to NULL.
   * Usually we don't close parent windows while child windows still live,
   * but sometimes it can happen. */
  if (self->parent && self->parent->children)
    {
      for (GSList *l = self->children; l; l = l->next)
        {
          XrdWindow *child = l->data;
          child->parent = NULL;
        }
    }
}

gboolean
xrd_window_has_parent (XrdWindow *self)
{
  return self->parent != NULL;
}

XrdWindow *
xrd_window_get_parent (XrdWindow *self)
{
  return self->parent;
}
