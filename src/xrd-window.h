/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef XRD_WINDOW_H_
#define XRD_WINDOW_H_

#if !defined(XRD_INSIDE) && !defined(XRD_COMPILATION)
#error "Only <xrd.h> can be included directly."
#endif

#include <g3k.h>

G_BEGIN_DECLS

#define XRD_TYPE_WINDOW xrd_window_get_type ()
G_DECLARE_FINAL_TYPE (XrdWindow, xrd_window, XRD, WINDOW, G3kPlane)

/**
 * XrdWindowRect:
 * @tl: The top left point of the rect
 * @bl: The bottom right point of the rect
 *
 * Describes for example a rectangle on a submitted texture.
 */
typedef struct
{
  struct
  {
    uint32_t x;
    uint32_t y;
  } bl;
  struct
  {
    uint32_t x;
    uint32_t y;
  } tr;
} XrdWindowRect;

XrdWindow *
xrd_window_new (G3kContext      *g3k,
                const gchar     *title,
                gpointer         native,
                VkExtent2D       size_pixels,
                graphene_size_t *size_meters);

void
xrd_window_set_rect (XrdWindow *self, XrdWindowRect *rect);

XrdWindowRect *
xrd_window_get_rect (XrdWindow *self);

void
xrd_window_get_pixel_intersection (XrdWindow          *self,
                                   graphene_point3d_t *intersection_3d,
                                   graphene_point_t   *intersection_pixels);

void
xrd_window_add_child (XrdWindow *self, XrdWindow *child);

void
xrd_window_set_selection_color (XrdWindow *self, gboolean is_selected);

gboolean
xrd_window_is_selected (XrdWindow *self);

void
xrd_window_reset_color (XrdWindow *self);

void
xrd_window_set_flip_y (XrdWindow *self, gboolean flip_y);

void
xrd_window_set_pin (XrdWindow *self, gboolean pinned, gboolean hide_unpinned);

gboolean
xrd_window_is_pinned (XrdWindow *self);

void
xrd_window_close (XrdWindow *self);

void
xrd_window_set_color (XrdWindow *self, const graphene_vec4_t *color);

gboolean
xrd_window_has_parent (XrdWindow *self);

XrdWindow *
xrd_window_get_parent (XrdWindow *self);

G_END_DECLS

#endif /* XRD_WINDOW_H_ */
